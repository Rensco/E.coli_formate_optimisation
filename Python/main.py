#!/usr/bin/env python3
"""
Loads an E. coli model, modifies it to grown on formate instead of glucose and finds knock-outs using OptKnock and
RobustKnock.
"""

import logging
import math
import multiprocessing
import typing

import cobra
import pandas
import scipy.stats
import statsmodels.sandbox.stats.multicomp

import gap_filler
import Optknock_robustknock

__author__ = "Rensco Hogers"


def add_missing_metabolites(file_name: str, model: cobra.core.Model) -> cobra.core.Model:
    """Opens a file containing all metabolites that need to be added, and adds them to the given model.

    :param file_name: str, name of the file that contains the metabolites that need to be added.
    :param model: cobra.core.Model, the model to which the metabolites will be added.
    :return: cobra.core.Model, updated model with the extra metabolites.
    """

    with open(file_name, "r", encoding="utf-8-sig") as metabolite_file:
        for line in metabolite_file:
            # skip empty lines
            if not (line := line.strip()):
                continue
            # Skip header line.
            if line.startswith("Metabolite_ID") or line.startswith(","):
                continue
            # Retrieve metabolite parameters from the file.
            metabolite_id, metabolite_name, metabolite_formula, metabolite_compartment = line.split(",")
            # Create a Cobra metabolite with the acquired parameters.
            metabolite = create_metabolite(metabolite_id, metabolite_name, metabolite_formula, metabolite_compartment)
            # Add the metabolite to the model.
            model.add_metabolites([metabolite])

    return model


def add_missing_reactions(file_name: str, model: cobra.core.Model) -> cobra.core.Model:
    """Opens the given file and extracts the reaction that need to be added to the given model.

    :param file_name: str, name of the file containing the missing reactions.
    :param model: cobra.core.Model, the model to which the reactions will be added.
    :return: cobra.core.Model, updated with the missing reactions.
    """

    with open(file_name, "r", encoding="utf-8-sig") as reaction_file:
        for line in reaction_file:
            # skip empty lines
            if not (line := line.strip()):
                continue
            # Skip header line.
            if line.startswith("Reaction_ID") or line.startswith(","):
                continue
            # Retrieve reaction parameters from the file.
            reaction_id, reaction_name, reaction_subsystem, reaction_lower_bound, reaction_upper_bound, \
            reaction_metabolites = line.split(",")
            # Create a Cobra reaction with the acquired parameters.
            reaction = create_reaction(reaction_id, reaction_name, reaction_subsystem, int(reaction_lower_bound),
                                       int(reaction_upper_bound))
            # Add metabolites to the reaction.
            reaction = add_reaction_metabolites(model, reaction, reaction_metabolites)
            model.add_reactions([reaction])

    return model


def add_reaction_metabolites(model: cobra.core.Model, reaction: cobra.Reaction, reaction_metabolites: str) \
        -> cobra.Reaction:
    """Add metabolites to a reaction and returns the updated cobra.reaction object.

    :param model: cobra.core.Model, used to retrieve metabolite parameters in the model.
    :param reaction: cobra.Reaction object, reaction to which metabolites will be added.
    :param reaction_metabolites: str, contains all metabolites and their kinetics that will be added to the reaction.
    :return: cobra.Reaction, updated with metabolites and kinetics.
    """

    # Create an empty dictionary to which metabolites and their kinetics will be added. Is needed because adding
    # metabolites to a reaction using the .add_metabolites function, requires a dictionary.
    metabolite_dict = {}
    # Split the string containing metabolites and their kinetics.
    for metabolite in reaction_metabolites.split(";"):
        metabolite = metabolite.strip()
        metabolite_id, kinetic = metabolite.split(":")
        metabolite = model.metabolites.get_by_id(metabolite_id)
        metabolite_dict[metabolite] = int(kinetic)

    # Add metabolites to the reaction.
    reaction.add_metabolites(metabolite_dict)

    return reaction


def adjust_medium(model: cobra.core.Model, metabolites: typing.List[typing.Tuple[str, float]]) -> cobra.core.Model:
    """Adjusts the given model based on the changes in the given list.

    :param model: cobra.core.Model, the model for which the medium will be adjusted.
    :param metabolites: list, containing a tuple with a reaction id as a string and the desired flux as a float.
    :return: cobra.core.Model, adjusted model.
    """

    medium = model.medium
    # Iterate over the list of tuples containing the reaction id and the desired flux.
    for reaction_id, flux in metabolites:
        medium[reaction_id] = flux
    model.medium = medium

    return model


def compare_fluxes(formate_fluxes: pandas.DataFrame, glucose_fluxes: pandas.DataFrame,
                   glycine_fluxes: pandas.DataFrame, pyruvate_fluxes: pandas.DataFrame, output_file: str) -> None:
    """Compares the fluxes of the formate, glucose, glycine and pyruvate reactions.

    :param formate_fluxes: pandas.DataFrame, fluxes of the formate reaction.
    :param glucose_fluxes: pandas.DataFrame, fluxes of the glucose reaction.
    :param glycine_fluxes: pandas.DataFrame, fluxes of the glycine reaction.
    :param pyruvate_fluxes: pandas.DataFrame, fluxes of the pyruvate reaction.
    :param output_file: str, location to which the flux analyses results will be saved to.
    :return: None.
    """

    # Calculate which reactions have a significantly different flux for the different substrates.
    rows = []
    for column in formate_fluxes:
        # Find the flux distributions for the different substrates, as well as the mean flux.
        formate_flux_distribution = formate_fluxes[column]
        mean_formate_flux = formate_flux_distribution.mean()

        glucose_flux_distribution = glucose_fluxes[column]
        mean_glucose_flux = glucose_flux_distribution.mean()

        glycine_flux_distribution = glycine_fluxes[column]
        mean_glycine_flux = glycine_flux_distribution.mean()

        pyruvate_flux_distribution = pyruvate_fluxes[column]
        mean_pyruvate_flux = pyruvate_flux_distribution.mean()

        # If all the flux distributions only have zeros, the column is skipped because it does not contain information.
        if mean_formate_flux == 0 and mean_glycine_flux == 0 and mean_pyruvate_flux == 0:
            continue

        # Perform the two-sample Kolmogorov-Smirnov test for goodness of fit to see if the two flux distributions are
        # similar.
        formate_glucose_ks_value, formate_glucose_p_value = scipy.stats.ks_2samp(formate_flux_distribution,
                                                                                 glucose_flux_distribution)

        formate_glycine_ks_value, formate_glycine_p_value = scipy.stats.ks_2samp(formate_flux_distribution,
                                                                                 glycine_flux_distribution)

        formate_pyruvate_ks_value, formate_pyruvate_p_value = scipy.stats.ks_2samp(formate_flux_distribution,
                                                                                   pyruvate_flux_distribution)

        row = {"Reaction_ID": column,
               "Mean_formate_flux": mean_formate_flux,
               "Mean_glucose_flux": mean_glucose_flux,
               "Mean_glycine_flux": mean_glycine_flux,
               "Mean_pyruvate_flux": mean_pyruvate_flux,
               "ks_value_formate_glucose": formate_glucose_ks_value,
               "p_value_formate_glucose": formate_glucose_p_value,
               "ks_value_formate_glycine": formate_glycine_ks_value,
               "p_value_formate_glycine": formate_glycine_p_value,
               "ks_value_formate_pyruvate": formate_pyruvate_ks_value,
               "p_value_formate_pyruvate": formate_pyruvate_p_value,
               }
        rows.append(row)
    significance = pandas.DataFrame(rows)

    # Multiple testing correction formate-glucose.
    p_adjusted_for_glu = statsmodels.sandbox.stats.multicomp.multipletests(
        significance["p_value_formate_glucose"], method="bonferroni", alpha=0.01)[1]

    # Multiple testing correction formate-glycine.
    p_adjusted_for_gly = statsmodels.sandbox.stats.multicomp.multipletests(
        significance["p_value_formate_glycine"], method="bonferroni", alpha=0.01)[1]

    # Multiple testing correction formate-pyruvate.
    p_adjusted_for_pyr = statsmodels.sandbox.stats.multicomp.multipletests(
        significance["p_value_formate_pyruvate"], method="bonferroni", alpha=0.01)[1]

    corrected_fluxsampling_results = pandas.DataFrame({"Reaction_ID": significance["Reaction_ID"],
                                                       "Mean_formate_flux": significance["Mean_formate_flux"],
                                                       "Mean_glucose_flux": significance["Mean_glucose_flux"],
                                                       "Mean_glycine_flux": significance["Mean_glycine_flux"],
                                                       "Mean_pyruvate_flux": significance["Mean_pyruvate_flux"],
                                                       "P_value_(KS)_For_Glu": p_adjusted_for_glu,
                                                       "P_value_(KS)_For_Gly": p_adjusted_for_gly,
                                                       "P_value_(KS)_For_Pyr": p_adjusted_for_pyr,
                                                       "KS_value_For_Glu": significance["ks_value_formate_glucose"],
                                                       "KS_value_For_Gly": significance["ks_value_formate_glycine"],
                                                       "KS_value_For_Pyr": significance["ks_value_formate_pyruvate"]
                                                       })

    rows = []
    for index, row in corrected_fluxsampling_results.iterrows():
        # Check if formate-glucose difference is significant.
        if row["P_value_(KS)_For_Glu"] > 0.01:
            formate_glucose_significant = False
        elif row["KS_value_For_Glu"] < 0.5:
            formate_glucose_significant = False
        elif not row["Mean_formate_flux"] >= 2 * row["Mean_glucose_flux"] and \
                not row["Mean_formate_flux"] <= 0.5 * row["Mean_glucose_flux"]:
            formate_glucose_significant = False
        else:
            formate_glucose_significant = True

        # Check if formate-glycine difference is significant.
        if row["P_value_(KS)_For_Gly"] > 0.01:
            formate_glycine_significant = False
        elif row["KS_value_For_Gly"] < 0.5:
            formate_glycine_significant = False
        elif not row["Mean_formate_flux"] >= 2 * row["Mean_glycine_flux"] and \
                not row["Mean_formate_flux"] <= 0.5 * row["Mean_glycine_flux"]:
            formate_glycine_significant = False
        else:
            formate_glycine_significant = True

        # Check if formate-pyruvate difference is significant.
        if row["P_value_(KS)_For_Pyr"] > 0.01:
            formate_pyruvate_significant = False
        elif row["KS_value_For_Pyr"] < 0.5:
            formate_pyruvate_significant = False
        elif not row["Mean_formate_flux"] >= 2 * row["Mean_pyruvate_flux"] and \
                not row["Mean_formate_flux"] <= 0.5 * row["Mean_pyruvate_flux"]:
            formate_pyruvate_significant = False
        else:
            formate_pyruvate_significant = True

        new_row = {"Reaction_ID": [row["Reaction_ID"]],
                   "Mean_formate_flux": [row["Mean_formate_flux"]],
                   "Mean_glucose_flux": [row["Mean_glucose_flux"]],
                   "Mean_glycine_flux": [row["Mean_glycine_flux"]],
                   "Mean_pyruvate_flux": [row["Mean_pyruvate_flux"]],
                   "P_value_(KS)_For_Glu": [row["P_value_(KS)_For_Glu"]],
                   "For_Glu_significant": formate_glucose_significant,
                   "P_value_(KS)_For_Gly": [row["P_value_(KS)_For_Gly"]],
                   "For_Gly_significant": formate_glycine_significant,
                   "P_value_(KS)_For_Pyr": [row["P_value_(KS)_For_Pyr"]],
                   "For_Pyr_significant": formate_pyruvate_significant
                   }
        rows.append(new_row)

    fluxsampling_results = pandas.DataFrame(rows)

    # Save the results, rounded to three decimals.
    fluxsampling_results.round(3).to_csv(output_file, index=False)

    return None


def create_metabolite(metabolite_id: str, metabolite_name: str, metabolite_formula: str,
                      metabolite_compartment: str) -> cobra.Metabolite:
    """Takes a cobrapy model and add the given metabolite data to add, and then returns the updated model.

    :param metabolite_id: str, the unique identifier of the new metabolite.
    :param metabolite_name: str, the name of the metabolite.
    :param metabolite_formula: str, the chemical formula of the metabolite.
    :param metabolite_compartment: str, cell compartment in which the metabolite resides.
    :return: cobra.Metabolite, new metabolite that can be added to a model.
    """

    metabolite = cobra.Metabolite(metabolite_id, formula=metabolite_formula, name=metabolite_name,
                                  compartment=metabolite_compartment)

    return metabolite


def create_reaction(reaction_id: str, reaction_name: str, reaction_subsystem: str = "",
                    reaction_lower_bound: int = -1000, reaction_upper_bound: int = 1000) -> cobra.Reaction:
    """Creates a new cobra.Reaction, to which the given reaction parameters will be assigned.

    :param reaction_id: str, unique identifier of the new reaction.
    :param reaction_name: str, name of the new reaction.
    :param reaction_subsystem: str, location in the cell in which the reaction takes place.
    :param reaction_lower_bound: int, the lowest allowed rate of the reaction. A negative number corresponds to the
    maximum allowed rate of the reaction in the reverse direction.
    :param reaction_upper_bound: int, the maximum rate of the reaction.
    :return: cobra.Reaction, with all the parameters assigned to it.
    """

    reaction = cobra.Reaction(reaction_id, name=reaction_name, subsystem=reaction_subsystem,
                              lower_bound=reaction_lower_bound, upper_bound=reaction_upper_bound)

    return reaction


def find_non_essential_reactions(model: cobra.core.Model) -> set:
    """Finds all reactions in the given model which are non-essential and are not associated with essential genes.

    :param model: cobra.core.Model, model from which the reactions will be retrieved.
    :return: set, containing all the reactions which can are non-essential and are not associated with essential genes.
    """

    model.objective = "BIOMASS_Ecoli_core_w_GAM"

    # Find essential reactions.
    reaction_deletion_model = cobra.flux_analysis.single_reaction_deletion(model, model.reactions[0:])
    essential_reactions = set()
    for ids in reaction_deletion_model:
        # Add reactions to the essential reaction set, if, when deleted, there is no growth.
        essential_reactions.update((model.reactions[i] for i in range(len(reaction_deletion_model[ids]))
                                    if reaction_deletion_model.growth[i] < 10E-06))

    # Find essential genes.
    gene_deletion_model = cobra.flux_analysis.single_gene_deletion(model, model.genes[0:])
    essential_genes = set()
    for ids in gene_deletion_model:
        # Add genes to the essential genes set, if, when deleted, there is no growth.
        essential_genes.update((model.reactions[i] for i in range(len(gene_deletion_model[ids]))
                                if gene_deletion_model.growth[i] < 10E-06))

    # Find all reactions which are not essential and add them to a new set.
    non_essential_reactions = set(model.reactions).difference(essential_reactions)

    # Find all non-essential reactions that are not associated with essential genes.
    possible_reactions = set(possible_reaction.id for possible_reaction in non_essential_reactions
                             if possible_reaction.genes not in essential_genes)

    return possible_reactions


def flux_analysis(model: cobra.core.Model, output_file: str, base_growth_rate, objective, substrate,
                  sample_size: int = 10000) -> pandas.DataFrame:
    """Runs flux analyses on the given cobrapy model and saves the results to a file.

    :param model: cobra.core.Model, for which flux analyses will be performed.
    :param output_file: str, location to which the flux analyses results will be saved to.
    :param base_growth_rate: float, base growth rate of the model.
    :param objective: str, objective reaction of the model.
    :param substrate: str, substrate reaction of the model.
    :param sample_size: int, number of samples to be drawn from the model.
    :return: pandas.DataFrame, flux analyses results.
    """

    model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = 0.98 * base_growth_rate

    # Use FBA to minimise the substrate influx for all models, in order to create a level playing field.
    # Find the needed substrate intake needed for the formate growth rate
    model.objective = objective
    # Reverse the solution, so it can be used to change the medium.
    intake = -model.optimize(objective_sense="maximize").objective_value
    medium_changes = [(substrate, intake)]
    model = adjust_medium(model, medium_changes)

    # Running flux sampling.
    fluxes = run_fluxsampling(model, sample_size, allowed_processors=6)
    fluxes.mean().to_csv(output_file)

    return fluxes


def load_model(model_file: str, file_type: str = "SBML") -> cobra.core.Model:
    """Loads the given model into cobrapy.

    :param model_file: str, name of the .xml model file that needs to be loaded.
    :param file_type, str, file_type in which the model file is given, (default smbl).
    :return: cobra.core.Model.
    """

    file_type = file_type.upper()

    if file_type == "SBML":
        return cobra.io.read_sbml_model(model_file)
    elif file_type == "JSON":
        return cobra.io.load_json_model(model_file)
    elif file_type == "YAML":
        return cobra.io.load_yaml_model(model_file)
    elif file_type == "MATLAB":
        return cobra.io.load_matlab_model(model_file)
    else:
        raise ValueError("Format should be one of: SBML, JSON, YAML or MATLAB")


def print_genes(model: cobra.core.Model) -> None:
    """Prints all the gene ids, names and associated reaction ids for the given model.

    :param model: cobra.core.Model, model of which the genes will be printed.
    :return: None.
    """

    for gene in model.genes:
        associated_reaction_ids = (reaction.id for reaction in gene.reactions)
        print(f"{gene.id}, {gene.name} is associated with reactions: {'{' + ', '.join(associated_reaction_ids) + '}'}")

    return None


def print_metabolites(model: cobra.core.Model) -> None:
    """Prints all the metabolite ids, names and their formula for the given model.

    :param model: cobra.core.Model, model of which the metabolites will be printed.
    :return: None.
    """

    for metabolite in model.metabolites:
        print(f"{metabolite.id}, {metabolite.name}, {metabolite.formula}")

    return None


def print_reactions(model: cobra.core.Model) -> None:
    """Prints all the reaction ids, names and reaction kinetics/bounds for the given model.

    :param model: cobra.core.Model, model of which the reactions will be printed.
    :return: None.
    """

    for reaction in model.reactions:
        print(f"{reaction.id}, {reaction.name} : {reaction.reaction}, {reaction.lower_bound}-{reaction.upper_bound}")

    return None


def run_fluxsampling(model: cobra.core.Model, sample_size: int, allowed_processors: int = multiprocessing.cpu_count()
                     ) -> pandas.DataFrame:
    """ Performs flux sampling for the given model, with the given sample size.

    :param model: cobra.core.Model, Model for which the samples will be calculated.
    :param sample_size: int, Number of samples that will be taken.
    :param allowed_processors: int, Number of processors the process is allowed to run on.
    :return: pandas.DataFrame, containing all the valid samples.
    """

    # If more cores are preferred than available, use all the available cores.
    if allowed_processors > multiprocessing.cpu_count():
        allowed_processors = multiprocessing.cpu_count()

    optgp = cobra.sampling.OptGPSampler(model, processes=allowed_processors, thinning=10)
    samples = optgp.sample(sample_size)
    # Check if all samples are feasible, and only continue with those samples and replace "Na" with zero.
    valid_sample_points = samples[optgp.validate(samples) == "v"].fillna(0.0)
    # Adjust all values below 1e-6 to 0.
    valid_sample_points[abs(valid_sample_points) < 1e-6] = 0

    return valid_sample_points


def run_gap_filler(model: cobra.core.Model, output_file_name: str, iterations: int = 5, max_reactions: int = 10) \
        -> None:
    """Attempts to find reactions that will increase the growth rate of the given model.

    :param model: cobra.core.Model, model to which new reactions will be added in order to improve the growth rate.
    :param output_file_name: str, location to which the gapfiller results will be saved to.
    :param iterations: int, number of different reaction that will be tried.
    :param max_reactions: int, number of reactions that can be added to the model at the same time.
    :return: None.
    """

    # Load the reference model containing all BIGG reactions.
    database = load_model("REFBIGG.xml", "SBML")

    # Filter reference database to only contain reactions that are not already in the model.
    gap_filler.delete_overlap(model, database)

    model.objective = "BIOMASS_Ecoli_core_w_GAM"

    # Combine model and reference database and initiate MILP.
    gf = gap_filler.GapFiller(model, universal=database, penalties=dict(universal=1, exchange=100, demand=1))

    # Run the gapfiller.
    new_id_counter = 0
    with open(output_file_name, "w+") as output_file:
        output_file.write("ID\tReaction(s)\tGrowth rate\n")
        for number_of_reactions in range(1, max_reactions + 1):
            gap_filler_output = gf.fill(iterations=iterations, num_reactions=number_of_reactions)
            # Write solution to file.
            for solution in gap_filler_output:
                output_file.write(f"ID_{(new_id_counter := new_id_counter + 1)}_r{number_of_reactions}\t"
                                  f"{', '.join([reaction.id for reaction in solution['reactions']])}\t"
                                  f"{solution['growth_rate']:.4f}\n")

    return None


def run_optknock_robustknock(model: cobra.core.Model, max_knock_outs: int = 20) -> None:
    """Runs OptKnock and RobustKnock on the given cobrapy model and prints which knock-outs have been found.

    :param model: cobra.core.Model, for which knock-outs will be made.
    :param max_knock_outs: int, maximal number of knock-outs allowed to be suggested.
    :return: None.
    """

    biomass_reaction = "BIOMASS_Ecoli_core_w_GAM"
    model.objective = biomass_reaction
    growth_rate = model.slim_optimize()
    target_reaction = biomass_reaction
    model.reactions.BIOMASS_Ecoli_core_w_GAM.lower_bound = growth_rate
    possible_reactions = find_non_essential_reactions(model)

    # Run OptKnock.
    number_of_knock_outs = 0
    while (number_of_knock_outs := number_of_knock_outs + 1) <= max_knock_outs:
        opt_r, rob_r = Optknock_robustknock.opt_and_rob(model, target_reaction, number_of_knock_outs,
                                                        possible_reactions)
        if not opt_r["KOs"] or not rob_r["KOs"]:
            print(f"Knock_out_number: {number_of_knock_outs}")
        elif round(opt_r["growth_rate"], 4) > round(growth_rate, 4) or \
                round(rob_r["growth_rate"], 4) > round(growth_rate, 4):
            print(f"{target_reaction}:{opt_r} | \t | {rob_r}")
        else:
            print(f"Knock_out_number: {number_of_knock_outs}")

    return None


def silence_warnings(package: str) -> None:
    """Takes a package and silences any warnings, errors will still be displayed.

    :param package: str, name of the package for which the warnings will be silenced.
    :return: None.
    """

    logging.getLogger(package).setLevel(logging.ERROR)

    return None


def main(errors_only: bool = True) -> None:
    """Executes the necessary functions in order to find knock-outs that improve biomass yield of E. coli on formate.

    :param errors_only: bool, default=True, if set to True it will suppress warnings thrown by cobra unless they are
    critical errors.
    """
    if errors_only:
        silence_warnings("cobra.io.sbml")

    # Files containing the names and associated info of the metabolites and reactions that are not in the model yet.
    missing_metabolites_file = "Metabolites_to_add.csv"
    missing_reactions_files = "Reactions_to_add.csv"

    # Using E. coli core-model.
    model_name = "E_coli_core_rGlyP.xml"

    formate_model = cobra.io.read_sbml_model(model_name)
    formate_model = add_missing_metabolites(missing_metabolites_file, formate_model)
    formate_model = add_missing_reactions(missing_reactions_files, formate_model)
    # formate_medium_changes = [("EX_for_e", 60)]
    formate_model.reactions.EX_for_e.lower_bound = -60
    formate_model.reactions.EX_for_e.upper_bound = -59
    # formate_model = adjust_medium(formate_model, formate_medium_changes)
    formate_model.objective = "BIOMASS_Ecoli_core_w_GAM"
    # formate_model.reactions.get_by_id("RPE").upper_bound = -0.225
    formate_model.reactions.get_by_id("FDH6r").upper_bound = 0.0
    formate_model.reactions.get_by_id("GLYTA").upper_bound = 0.0
    # formate_model.optimize()
    # print(formate_model.reactions.RPE.summary())

    growth_rate_formate = formate_model.slim_optimize()
    doubling_time_formate = math.log(2) / math.log(1 + (growth_rate_formate / 100))
    specific_growth_yield_formate = growth_rate_formate * 1000 / 60
    print(f"Unoptimised growth rate on formate: {growth_rate_formate:.4f} 1/h")
    print(f"Unoptimised doubling time on formate: {doubling_time_formate:.4f} h")
    print(f"Unoptimised specific growth yield formate: {specific_growth_yield_formate:.4f} g/mol")
    print("\n")
    # solution = formate_model.optimize()
    # solution_dataframe = pandas.DataFrame.from_dict(solution.fluxes)
    # solution_dataframe.to_csv("Fluxes_without_GLYTA.csv")
    # exit()

    # cobra.io.save_json_model(formate_model, "ecoli_core_with_rGlyP.json")

    # Run OptKnock and RobustKnock to find knock-out candidates.
    # Run_optknock_robustknock(formate_model)

    # Running gap filling.
    # gapfiller_output_file = "Output_gapfiller.txt"
    # run_gap_filler(formate_model, gapfiller_output_file)

    glucose_model = load_model(model_name, "SBML")
    glucose_model.solver = "gurobi"
    glucose_model = add_missing_metabolites(missing_metabolites_file, glucose_model)
    glucose_model = add_missing_reactions(missing_reactions_files, glucose_model)
    glucose_medium_changes = [("EX_glc__D_e", 10)]
    glucose_model = adjust_medium(glucose_model, glucose_medium_changes)
    glucose_model.objective = "BIOMASS_Ecoli_core_w_GAM"
    glucose_model.reactions.get_by_id("FDH6r").upper_bound = 0.0
    glucose_model.reactions.get_by_id("GLYTA").upper_bound = 0.0
    growth_rate_glucose = glucose_model.slim_optimize()
    doubling_time_glucose = math.log(2) / math.log(1 + (growth_rate_glucose / 100))
    specific_growth_yield_glucose = growth_rate_glucose * 1000 / 10
    print(f"Unoptimised growth rate on glucose: {growth_rate_glucose:.4f} 1/h")
    print(f"Unoptimised doubling time on glucose: {doubling_time_glucose:.4f} h")
    print(f"Unoptimised specific growth yield glucose: {specific_growth_yield_glucose:.4f} g/mol")
    print("\n")

    glycine_model = load_model(model_name, "SBML")
    glycine_model.solver = "gurobi"
    glycine_model = add_missing_metabolites(missing_metabolites_file, glycine_model)
    glycine_model = add_missing_reactions(missing_reactions_files, glycine_model)
    glycine_model_medium_changes = [("EX_gly_e", 30)]
    glycine_model = adjust_medium(glycine_model, glycine_model_medium_changes)
    glycine_model.objective = "BIOMASS_Ecoli_core_w_GAM"
    glycine_model.reactions.get_by_id("FDH6r").upper_bound = 0.0
    glycine_model.reactions.get_by_id("GLYTA").upper_bound = 0.0
    growth_rate_glucose = glycine_model.slim_optimize()
    doubling_time_glucose = math.log(2) / math.log(1 + (growth_rate_glucose / 100))
    specific_growth_yield_glucose = growth_rate_glucose * 1000 / 10
    print(f"Unoptimised growth rate on glycine: {growth_rate_glucose:.4f} 1/h")
    print(f"Unoptimised doubling time on glycine: {doubling_time_glucose:.4f} h")
    print(f"Unoptimised specific growth yield glycine: {specific_growth_yield_glucose:.4f} g/mol")
    print("\n")

    pyruvate_model = load_model(model_name, "SBML")
    pyruvate_model.solver = "gurobi"
    pyruvate_model = add_missing_metabolites(missing_metabolites_file, pyruvate_model)
    pyruvate_model = add_missing_reactions(missing_reactions_files, pyruvate_model)
    pyruvate_medium_changes = [("EX_pyr_e", 20)]
    pyruvate_model = adjust_medium(pyruvate_model, pyruvate_medium_changes)
    pyruvate_model.objective = "BIOMASS_Ecoli_core_w_GAM"
    pyruvate_model.reactions.get_by_id("FDH6r").upper_bound = 0.0
    pyruvate_model.reactions.get_by_id("GLYTA").upper_bound = 0.0
    growth_rate_glucose = pyruvate_model.slim_optimize()
    doubling_time_glucose = math.log(2) / math.log(1 + (growth_rate_glucose / 100))
    specific_growth_yield_glucose = growth_rate_glucose * 1000 / 10
    print(f"Unoptimised growth rate on pyruvate: {growth_rate_glucose:.4f} 1/h")
    print(f"Unoptimised doubling time on pyruvate: {doubling_time_glucose:.4f} h")
    print(f"Unoptimised specific growth yield pyruvate: {specific_growth_yield_glucose:.4f} g/mol")
    print("\n")

    formate_fluxes = flux_analysis(model=formate_model, output_file="Formate_fluxes.csv",
                                   base_growth_rate=growth_rate_formate, objective="EX_for_e", substrate="EX_for_e")
    glucose_fluxes = flux_analysis(model=formate_model, output_file="Glucose_fluxes.csv",
                                   base_growth_rate=growth_rate_formate, objective="EX_glc__D_e",
                                   substrate="EX_glc__D_e")
    glycine_fluxes = flux_analysis(model=formate_model, output_file="Glycine_fluxes.csv",
                                   base_growth_rate=growth_rate_formate, objective="EX_gly_e", substrate="EX_gly_e")
    pyruvate_fluxes = flux_analysis(model=formate_model, output_file="Pyruvate_fluxes.csv",
                                    base_growth_rate=growth_rate_formate, objective="EX_pyr_e", substrate="EX_pyr_e")

    compare_fluxes(formate_fluxes, glucose_fluxes, glycine_fluxes, pyruvate_fluxes, "Flux_sampling_results.csv")

    return None


if __name__ == "__main__":
    main()
