#!/usr/bin/env python3
"""
ADAPTED FROM COBRAPY 0.22.1 gap filling. The changes allow to preset the number of reaction additions and optimise for
growth rate, rather than add a minimised number of reactions to obtain a predefined minimal growth rate.
"""

import logging
import typing

import cobra
import optlang
import sympy


class GapFiller:
    r"""
    The base class for performing gap filling. This class implements gap filling based on a mixed-integer approach,very
    similar to that described in [1]_ and the 'no-growth but growth' part of [2]_ but with minor adjustments.
    In short, we add indicator variables for using the reactions in the universal model, z_i and then solve problem:
    minimize: \sum_i c_i * z_i
    s.t.    : Sv = 0
              v_o \ge t
              lb_i \le v_i \le ub_i
              v_i = 0 if z_i = 0
    where lb, ub are respectively the upper, lower flux bounds for reaction i, c_i is a cost parameter and the objective
    v_o is greater than the lower bound t. The default costs are 1 for reactions from the universal model, 100 for
    exchange (uptake) reactions added and 1 for added demand reactions. Note that this is a mixed-integer linear program
    and as such will be expensive to solve for large models. Consider using alternatives [3], such as CORDA instead
    [4,5].
    :param model: cobra.Model, The model to perform gap filling on.
    :param universal : cobra.Model, optional, A universal model with reactions that can be used to complete the `model`
    (default None).
    :param lower_bound : float, optional, The minimally accepted flux for the objective in the filled model
    (default 0.05).
    :param penalties : dict, optional, A dictionary with keys being 'universal' (all reactions included in the universal
    model), 'exchange' and 'demand' (all additionally added exchange and demand reactions) for the three reaction types.
    Can also have reaction identifiers for reaction specific costs. Defaults are 1, 100 and 1 respectively
    (default None).
    :param exchange_reactions : bool, optional, Consider adding exchange (uptake) reactions for all metabolites in the
    model (default False).
    :param demand_reactions : bool, optional, Consider adding demand reactions for all metabolites (default True).
    :param integer_threshold : float, optional, The threshold at which a value is considered non-zero (aka integrality
    threshold). If gap filled models fail to validate, you may want to lower this value (default 1E-6).

    :ivar indicators: list of optlang.interface.Variable, the list of symbolic indicator variables.
    :ivar costs: dict of {optlang.interface.Variable: float}, The dictionary with symbolic variables as keys and their
    cost as values.
    References
    ----------
    [1] Reed, Jennifer L., Trina R. Patel, Keri H. Chen, Andrew R. Joyce, Margaret K. Applebee, Christopher D. Herring,
    Olivia T. Bui, Eric M. Knight, Stephen S. Fong, and Bernhard O. Palsson.“Systems Approach to Refining Genome
    Annotation.” Proceedings of the National Academy of Sciences 103, no. 46 (2006): 17480–17484.
    [2] Kumar, Vinay Satish, and Costas D. Maranas. “GrowMatch: An Automated Method for Reconciling In Silico/In Vivo
    Growth Predictions.” Edited by Christos A. Ouzounis. PLoS Computational Biology 5, no. 3 (March 13, 2009): e1000308.
    doi:10.1371/journal.pcbi.1000308.
    [3] https://opencobra.github.io/cobrapy/tags/gapfilling/
    [4] Schultz, André, and Amina A. Qutub. “Reconstruction of Tissue-Specific Metabolic Networks Using CORDA.”
    Edited by Costas D. Maranas. PLOS Computational Biology 12, no. 3 (March 4, 2016): e1004808.
    doi:10.1371/journal.pcbi.1004808.
    [5] Diener, Christian https://github.com/cdiener/corda
    """

    def __init__(self, model: cobra.core.Model, universal: cobra.core.Model = None, lower_bound: float = 0.05,
                 penalties: typing.Dict[str, int] = None, exchange_reactions: bool = False,
                 demand_reactions: bool = True, integer_threshold: float = 1e-6) -> None:
        """Initialize a new GapFiller object.
        """

        self.original_model = model
        self.lower_bound = lower_bound
        self.model = model.copy()
        tolerances = self.model.solver.configuration.tolerances
        try:
            tolerances.integrality = integer_threshold
        except AttributeError:
            logging.getLogger(__name__).warning(f"The current solver interface"
                                                f"{cobra.util.interface_to_str(self.model.problem)} does not support "
                                                f"setting the integrality tolerance.")
        self.universal = universal.copy() if universal else cobra.core.Model("universal")
        self.penalties = dict(universal=1, exchange=100, demand=1)
        if penalties is not None:
            self.penalties.update(penalties)
        self.integer_threshold = integer_threshold
        self.indicators = []
        self.costs = {}
        self.extend_model(exchange_reactions, demand_reactions)
        cobra.util.fix_objective_as_constraint(self.model, bound=lower_bound)

        # Remove dead-end reactions and metabolites.
        self.remove_dead_ends()

        self.add_switches_and_objective()

    def extend_model(self, exchange_reactions: bool = False, demand_reactions: bool = True) -> None:
        """Extend gap filling model.

        Add reactions from universal model and optionally exchange and demand reactions for all metabolites in the model
        to perform gap filling on.

        :param exchange_reactions : bool, Consider adding exchange (uptake) reactions for all metabolites in the model.
        :param demand_reactions : bool, consider adding demand reactions for all metabolites.
        :return: None.
        """

        for reaction in self.universal.reactions:
            reaction.gapfilling_type = "universal"
        new_metabolites = self.universal.metabolites.query(lambda metabolite: metabolite not in self.model.metabolites)
        self.model.add_metabolites(new_metabolites)
        existing_exchanges = []
        for reaction in self.universal.boundary:
            existing_exchanges = existing_exchanges + [metabolite.id for metabolite in list(reaction.metabolites)]

        for metabolite in self.model.metabolites:
            if exchange_reactions:
                reaction = self.universal.add_boundary(metabolite, type="exchange_smiley", lb=-1000, ub=0,
                                                       reaction_id=f"EX_{metabolite.id}")
                reaction.gapfilling_type = "exchange"
            if demand_reactions:
                reaction = self.universal.add_boundary(metabolite, type="demand_smiley", lb=0, ub=1000,
                                                       reaction_id=f"DM_{metabolite.id}")
                reaction.gapfilling_type = "demand"

        new_reactions = self.universal.reactions.query(lambda new_reaction: new_reaction not in self.model.reactions)
        self.model.add_reactions(new_reactions)

    def remove_dead_ends(self) -> None:
        """Removes dead-end metabolites and reactions.
        """

        import time
        start = time.time()
        # Remove dead-end metabolites.
        while True:
            print(f"{len(self.model.metabolites)} metabolites in model.")
            dead_end_metabolites = self.model.metabolites.query(lambda metabolite: len(metabolite.reactions) == 1)
            if not dead_end_metabolites:
                break
            self.model.remove_metabolites(dead_end_metabolites, destructive=True)
            print(f"Removed {len(dead_end_metabolites)} dead-end metabolites.")

        print(f"{time.time() - start:.2f} seconds to remove dead-end metabolites.")

        print("Switching to only consumable/producible metabolites")
        while True:
            print(f"Number of reactions {len(self.model.reactions):d}")
            met_list = [metabolite for metabolite in self.model.metabolites if len(metabolite.reactions) == 1
                        # Not producible
                        or not any([reaction for reaction in self.model.reactions if metabolite in reaction.metabolites
                                    and ((reaction.upper_bound > 0 and reaction.metabolites[metabolite] > 0) or
                                         (reaction.lower_bound < 0 and reaction.metabolites[metabolite] < 0))])
                        # Not consumable
                        or not any([reaction for reaction in self.model.reactions if metabolite in reaction.metabolites
                                    and ((reaction.upper_bound > 0 > reaction.metabolites[metabolite])
                                         or (reaction.lower_bound < 0 < reaction.metabolites[metabolite]))])]

            if not met_list:
                break

            print(f"Removing {len(met_list):d} dead-end metabolites")
            self.model.remove_metabolites(met_list, destructive=True)

        return None

    def update_costs(self) -> None:
        """Update coefficients for the indicator variables in the objective.

        Done incrementally so that second time the function is called, active indicators in the current solutions gets
        higher cost than the unused indicators.
        """

        for variable in self.indicators:
            if variable not in self.costs:
                self.costs[variable] = variable.cost
            else:
                if variable._get_primal() > self.integer_threshold:
                    self.costs[variable] += variable.cost
        self.model.objective.set_linear_coefficients(self.costs)

        return None

    def add_switches_and_objective(self) -> None:
        """Update gap filling model with switches and indicator objective."""

        constraints = []
        big_m = max(max(abs(bound) for bound in reaction.bounds) for reaction in self.model.reactions)
        prob = self.model.problem
        for reaction in self.model.reactions:
            if not hasattr(reaction, "gapfilling_type"):
                continue
            indicator = prob.Variable(name=f"indicator_{reaction.id}", lb=0, ub=1, type="binary")
            if reaction.id in self.penalties:
                indicator.cost = self.penalties[reaction.id]
            else:
                indicator.cost = self.penalties[reaction.gapfilling_type]
            indicator.reaction_id = reaction.id
            self.indicators.append(indicator)

            # if z = 1 v_i is allowed non-zero
            # v_i - Mz <= 0   and   v_i + Mz >= 0
            constraint_lb = prob.Constraint(reaction.flux_expression - big_m * indicator, ub=0,
                                            name="constraint_lb_{}".format(reaction.id), sloppy=True)
            constraint_ub = prob.Constraint(reaction.flux_expression + big_m * indicator, lb=0,
                                            name="constraint_ub_{}".format(reaction.id), sloppy=True)

            constraints.extend([constraint_lb, constraint_ub])

        # Add constraint on number of added reactions
        constraint_num_reactions = self.model.problem.Constraint(sympy.Add(*self.indicators), lb=0, ub=0,
                                                                 name='max_reactions')
        constraints.append([constraint_num_reactions])

        self.model.add_cons_vars(self.indicators)
        self.model.add_cons_vars(constraints, sloppy=True)

    def fill(self, iterations: int = 1, num_reactions: int = 3) -> \
            typing.List[typing.Dict[str, typing.Union[list, typing.Any]]]:
        """Perform the gap filling. With every iteration, it solves the model, updates the costs and records the used
        reactions.

        :param iterations: int, optional, the number of rounds of gap filling to perform. For every iteration,
        the penalty for every used reaction increases linearly. This way, the algorithm is encouraged to search for
        alternative solutions which may include previously used reactions i.e., with enough iterations pathways
        including 10 steps will eventually be reported even if the shortest pathway is a single reaction (default 1).
        :param num_reactions: int, optional, number of reactions that are allowed to be added to the model at once
        (default 3).
        :return list of lists of cobra.Reaction. A list of lists where each element is a list of reactions that were
        used to gap fill the model.
        :raise RuntimeError: If the model fails to be validated (i.e. the original model with the proposed reactions
        added, still cannot get the required flux through the objective).
        """

        self.remove_integer_cuts()

        # Update number of reaction additions (in order, code crashes if lb>ub...)
        if num_reactions < self.model.constraints.max_reactions.lb:
            self.model.constraints.max_reactions.lb = num_reactions
            self.model.constraints.max_reactions.ub = num_reactions
        else:
            self.model.constraints.max_reactions.ub = num_reactions
            self.model.constraints.max_reactions.lb = num_reactions

        used_reactions = []
        for iteration in range(iterations):
            growth_rate = self.model.slim_optimize(error_value=None, message="gap filling optimization failed")
            solution = {"growth_rate": growth_rate, "reactions": [self.model.reactions.get_by_id(indicator.reaction_id)
                                                                  for indicator in self.indicators
                                                                  if indicator._get_primal() > self.integer_threshold]}
            if not self.validate(solution):
                raise RuntimeError("Failed to validate gap filled model, try lowering the integer threshold.")
            used_reactions.append(solution)

            sol_ind = [indicator for indicator in self.indicators if indicator._get_primal() > self.integer_threshold]
            int_cut = self.model.problem.Constraint(sympy.Add(*sol_ind), lb=0, ub=len(sol_ind) - 0.5,
                                                    name=f'integer_cut_{iteration}')
            self.model.add_cons_vars(int_cut, sloppy=True)

        return used_reactions

    def validate(self, reactions: typing.Dict[str, typing.Union[list, typing.Any]]) -> bool:
        """Validate the model.

        :param reactions: List, list of cobra.Reaction. The reactions to add to the model for validation.
        :return: bool, whether the model is valid or not.
        """

        with self.original_model as model:
            model.add_reactions(reactions["reactions"])
            model.slim_optimize()
            return (model.solver.status == optlang.interface.OPTIMAL and
                    model.solver.objective.value >= self.lower_bound)

    def remove_integer_cuts(self) -> None:
        """Removes constrains that have been annotated as having an integer cut.

        :return: None.
        """

        self.model.remove_cons_vars([constraint for constraint in self.model.constraints
                                     if "integer_cut_" in constraint.name])

        return None


def delete_overlap(model: cobra.core.Model, reference: cobra.core.Model) -> None:
    """deletes reactions from reference which also occur in the model. Necessary because gap filling algorithm cannot
    handle it if a reaction occurs in both.

    :param model: cobra.core.Model, model which is used to simulate an organism.
    :param reference: cobra.core.Model, database model with all the reactions that can be added.
    :return: None.
    """

    duplicate_reactions = [reaction for reaction in reference.reactions if reaction.id in model.reactions]
    reference.remove_reactions(duplicate_reactions)

    return None


def same_reaction(reaction_a, reaction_b) -> bool:
    """checks if two reactions are exactly the same.

    :param reaction_a: cobra.core.reaction.Reaction, reaction which will be compared to the other given reaction.
    :param reaction_b: cobra.core.reaction.Reaction, reaction which will be compared to the other given reaction.
    :return: bool, True if the two reactions are the same, and False if the two reaction are not the same.
    """

    a_set = set((metabolite.id, reaction_a.metabolites[metabolite]) for metabolite in reaction_a.metabolites)
    b_set = set((metabolite.id, reaction_b.metabolites[metabolite]) for metabolite in reaction_b.metabolites)

    return a_set == b_set
