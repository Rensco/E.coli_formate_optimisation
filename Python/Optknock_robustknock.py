#!/usr/bin/env python3
"""
Original author: Ruben

Updated by Sara Benito 05/04/18. Master Thesis 2018- Metabolic modelling of Actinobacteria.
Updated by Rensco hogers 2021/10/05. Master Thesis 2021- Model-driven optimisation of synthetic formatotrophy.
Script to run OptKnock and RobustKnock algorithms that gives the possible knock-outs which would lead to the maximum
production of a target reaction.
Example:
    targets_reactions=["EX_ba"].
    model=cobra.io.read_sbml_model("modelo_prueba_ba.xml").
    Opt_R,Rob_R=OptAndRob(model,targets_reactions,5).

Script works for latest cobrapy version 0.22.1 after the following modifications:
    -model.solution.f replace by model.optimize().objective_value.
    -The chosen solver is gurobi (gurobi 9.1.2). Cplex optimizer does give a null solution at every knock out
    selection. The cplex for python is not recognized when is called.
    -tilt="yes"/"no" replace as suggested by "True"/"False".
    -from cobra.core.Solution import Solution replaced by from cobra.core.solution import Solution.
    -function solve(): 4 arguments might be included in the Solution class call instead of two: self, objective_value,
    status and fluxes.
    The self.solution.x and self.solution.y assignments have been commented (those arguments are deprecated and they are
    not used later on).
    -In function evaluateKO, FVA variable is slightly modified (see function) and minval, maxval it is now defined as:
    minval=FVA.minimum[targetRxn] maxval=FVA.maximum[targetRxn].
"""
from typing import Any

import cobra
import copy
import pulp
import typing


# TO DO
# Try accessing Cplex directly as alternative to OptSlope code.
# Replace "yes"/"no" tilt by True/False.
# KO_cand is sometimes "" and sometimes None --> Standardise.
# Add integer cuts.
# Allow single reaction (non-list) to be added in OptAndRob.


class OptKnock:
    """OptKnock class.
    """

    # Code originally from OptSlope (Elad Noor).
    # Modifications:
    # 1) Changed use_glpk to solver_choice.
    # 2) Removed OptSlope.
    # 3) Removed Solve_FVA.
    # 4) Removed prepare_FBA.
    # 5) Removed functions to print results.
    # 6) Removed get_reaction_by_id function - only used once, cobra_model already has a get_by_id function.
    # 7) Added objective tilting.

    def __init__(self, model: cobra.core.Model, verbose: bool = False) -> None:
        """Constructor method.
        """

        self.model = copy.deepcopy(model)
        self.verbose = verbose

        # locate the biomass reaction.
        biomass_reactions = [reaction for reaction in self.model.reactions if reaction.objective_coefficient != 0]
        if len(biomass_reactions) != 1:
            raise Exception("There should be only one biomass reaction")
        self.biomass_reaction = biomass_reactions[0]

        self.has_flux_as_variables = False

        self.prob = None
        self.var_v = None
        self.var_lambda = None
        self.var_w_u = None
        self.var_w_l = None
        self.var_y = None
        self.var_mu = None
        self.target_reaction = None
        self.solution = None

    def prepare_optknock(self, target_reaction_id: str, ko_candidates: list = None, num_deletions: int = 5,
                         solver_choice: str = "gurobi", tilt: bool = False, tilt_value: float = 0.00001) -> None:
        """ Prepare the model for OptKnock.

        :param target_reaction_id: str, the id of the target reaction.
        :param ko_candidates: list, the list of candidate reactions to knockout.
        :param num_deletions: int, the number of deletions to perform.
        :param solver_choice: str, the solver to use.
        :param tilt: bool, whether to use objective tilting.
        :param tilt_value: float, the value to use for objective tilting.
        :return None.
        """

        # find and assign the target reaction.
        self.target_reaction = self.model.reactions.get_by_id(target_reaction_id)

        self.create_prob(sense=pulp.LpMaximize, solver_choice=solver_choice)
        self.add_primal_variables_and_constraints()
        self.add_dual_variables_and_constraints()

        if tilt:
            # for metabolite, coefficient in self.target_reaction._metabolites.iteritems().
            s_times_lambda = pulp.LpAffineExpression([(self.var_lambda[metabolite], coefficient)
                                                      for metabolite, coefficient in
                                                      self.target_reaction.metabolites.items() if coefficient != 0])
            row_sum = s_times_lambda + self.var_w_u[self.target_reaction] - self.var_w_l[self.target_reaction]
            self.prob.constraints[f"dual_{self.target_reaction.id}"] = (row_sum == -tilt_value)

        self.add_optknock_variables_and_constraints(tilt=tilt, tilt_value=tilt_value)

        # Add the objective of maximizing the flux in the target reaction.
        self.prob.setObjective(self.var_v[self.target_reaction])

        self.add_knockout_bounds(ko_candidates, num_deletions)

        return None

    def create_prob(self, sense: int = pulp.LpMaximize, solver_choice: str = "gurobi") -> None:
        """ Create the problem.

        :param sense: int, pulp.LpMaximize or pulp.LpMinimize.
        :param solver_choice: str, "glpk", "gurobi" or "cplex".
        :return None.
        """

        # create the LP.
        self.prob = pulp.LpProblem("OptKnock", sense=sense)
        if solver_choice == "glpk":
            self.prob.solver = pulp.apis.GLPK()
        elif solver_choice == "gurobi":
            self.prob.solver = pulp.apis.GUROBI(msg=self.verbose)
        elif solver_choice == "cplex":
            self.prob.solver = pulp.apis.CPLEX(msg=self.verbose)
        else:
            raise Exception(f"{solver_choice} solver not among available options.")
        if not self.prob.solver.available():
            raise Exception(f"{solver_choice} not available.")

        return None

    def add_primal_variables_and_constraints(self) -> None:
        """ Add primal variables and constraints.

        :return None.
        """

        # Create the continuous flux variables (can be positive or negative).
        self.var_v = {}
        for reaction in self.model.reactions:
            self.var_v[reaction] = pulp.LpVariable(f"v_{reaction.id}", lowBound=reaction.lower_bound,
                                                   upBound=reaction.upper_bound, cat=pulp.LpContinuous)

        # this flag will be used later to know if to expect the flux variables to exist.
        self.has_flux_as_variables = True

        # add the mass-balance constraints to each of the metabolites (S*v = 0).
        for metabolite in self.model.metabolites:
            s_times_v = pulp.LpAffineExpression([(self.var_v[r], r.get_coefficient(metabolite)) for r in
                                                 metabolite.reactions])
            self.prob.addConstraint(s_times_v == 0, f"mass_balance_{metabolite.id}")

        return None

    def add_dual_variables_and_constraints(self, m: int = 1000) -> None:
        """ Add dual variables and constraints.

        :param m: int, the number of dual variables to create.
        :return None.
        """

        # create dual variables associated with stoichiometric constraints.
        self.var_lambda = dict([(metabolite, pulp.LpVariable(f"lambda_{metabolite.id}", lowBound=-m, upBound=m,
                                                             cat=pulp.LpContinuous))
                                for metabolite in self.model.metabolites])

        # create dual variables associated with the constraints on the primal fluxes.
        self.var_w_u = dict([(reaction, pulp.LpVariable(f"w_U_{reaction.id}", lowBound=0, upBound=m,
                                                        cat=pulp.LpContinuous))
                             for reaction in self.model.reactions])
        self.var_w_l = dict([(reaction, pulp.LpVariable(f"w_L_{reaction.id}", lowBound=0, upBound=m,
                                                        cat=pulp.LpContinuous))
                             for reaction in self.model.reactions])

        # add the dual constraints:
        #   S"*lambda + w_U - w_L = c_biomass.
        for reaction in self.model.reactions:
            # for metabolite, coefficient in r._metabolites.iteritems().
            s_times_lambda = pulp.LpAffineExpression([(self.var_lambda[metabolite], coefficient)
                                                      for metabolite, coefficient in reaction.metabolites.items()
                                                      if coefficient != 0])
            row_sum = s_times_lambda + self.var_w_u[reaction] - self.var_w_l[reaction]
            self.prob.addConstraint(row_sum == reaction.objective_coefficient, f"dual_{reaction.id}")

        return None

    def add_optknock_variables_and_constraints(self, tilt: bool = False, tilt_value: float = 0.00001, m: int = 1000) \
            -> None:
        """ Add the variables and constraints for the OptKnock algorithm.

        :param tilt: bool, if True, the algorithm will use the tilt method.
        :param tilt_value: float, the value of the tilt parameter.
        :param m: int, the number of dual variables to create.
        :return None.
        """

        # create the binary variables indicating which reactions knocked out.
        self.var_y = dict([(reaction, pulp.LpVariable(f"y_{reaction.id}", cat=pulp.LpBinary))
                           for reaction in self.model.reactions])

        # create dual variables associated with the constraints on the primal fluxes.
        self.var_mu = dict([(reaction, pulp.LpVariable(f"mu_{reaction.id}", cat=pulp.LpContinuous)) for reaction in
                            self.model.reactions])

        # equate the objectives of the primal and the dual of the inner problem
        # to force its optimization: sum_j mu_j - v_biomass = 0
        if tilt:
            constraint = (
                    pulp.lpSum(self.var_mu.values()) - self.var_v[self.biomass_reaction] + tilt_value *
                    self.var_v[self.target_reaction] == 0)
        else:
            constraint = (pulp.lpSum(self.var_mu.values()) - self.var_v[self.biomass_reaction] == 0)
        self.prob.addConstraint(constraint, "dual_equals_primal")

        # add the knockout constraints (when y_j = 0, v_j has to be 0).
        for reaction in self.model.reactions:
            # L_jj * y_j <= v_j
            self.prob.addConstraint(reaction.lower_bound * self.var_y[reaction] <= self.var_v[reaction],
                                    f"v_lower_{reaction.id}")
            # v_j <= U_jj * y_j
            self.prob.addConstraint(self.var_v[reaction] <= reaction.upper_bound * self.var_y[reaction],
                                    f"v_upper_{reaction.id}")

        # set the constraints on the auxiliary variables (mu): mu_j == y_j * (U_jj * w_u_j - L_jj * w_l_j).
        for reaction in self.model.reactions:
            w_sum = pulp.LpAffineExpression([(self.var_w_u[reaction], reaction.upper_bound), (self.var_w_l[reaction],
                                                                                              -reaction.lower_bound)])

            # mu_j + M*y_j >= 0.
            self.prob.addConstraint(self.var_mu[reaction] + m * self.var_y[reaction] >= 0, f"aux_1_{reaction.id}")
            # -mu_j + M*y_j >= 0.
            self.prob.addConstraint(-self.var_mu[reaction] + m * self.var_y[reaction] >= 0, f"aux_2_{reaction.id}")
            # mu_j - (U_jj * w_u_j - L_jj * w_l_j) + M*(1-y_j) >= 0.
            self.prob.addConstraint(self.var_mu[reaction] - w_sum + m * (1 - self.var_y[reaction]) >= 0,
                                    f"aux_3_{reaction.id}")
            # -mu_j + (U_jj * w_u_j - L_jj * w_l_j) + M*(1-y_j) >= 0.
            self.prob.addConstraint(-self.var_mu[reaction] + w_sum + m * (1 - self.var_y[reaction]) >= 0,
                                    f"aux_4_{reaction.id}")

        return None

    def add_knockout_bounds(self, ko_candidates: list = None, num_deletions: int = 5) -> None:
        """Construct the list of knock-out candidates and add a constraint that only K (num_deletions) of them can have
        a y_j = 0

        :param ko_candidates: list, the list of reactions to be knocked out.
        :param num_deletions: int, the number of reactions to be knocked out.
        :return None.
        """

        if ko_candidates is None:
            ko_candidates = [reaction for reaction in self.model.reactions if reaction != self.biomass_reaction]

        for reaction in set(self.model.reactions).difference(ko_candidates):
            # if "reaction" is not a knock-out candidate constrain it to be "active".
            # i.e.   y_j == 1.
            self.prob.addConstraint(self.var_y[reaction] == 1, f"active_{reaction.id}")

        # set the upper bound on the number of knockouts (K).
        # sum (1 - y_j) <= K.
        ko_candidate_sum_y = [(self.var_y[reaction], 1) for reaction in ko_candidates]
        # constraint = (pulp.LpAffineExpression(ko_candidate_sum_y) >= len(ko_candidate_sum_y) - num_deletions).
        constraint = (pulp.LpAffineExpression(ko_candidate_sum_y) >= len(ko_candidate_sum_y) - num_deletions)
        self.prob.addConstraint(constraint, "number_of_deletions")

        return None

    def solve(self) -> cobra.core.solution.Solution:
        """ Solve the LP problem.

        :return: cobra.core.solution.Solution.
        """

        self.prob.solve()

        if self.prob.status != pulp.LpStatusOptimal:
            if self.verbose:
                print(f"LP was not solved because: {pulp.LpStatus[self.prob.status]}")
            # It needs 4 arguments as well.
            self.solution = cobra.core.solution.Solution(fluxes=self.prob.objective.value(), objective_value=0,
                                                         status=self.prob.status)
        else:
            # It needs 4 arguments: self, objective_value,status, fluxes. Fluxes is a panda.Series, contains the
            # reaction fluxes (primal values of variables).
            self.solution = cobra.core.solution.Solution(self.prob.objective.value(), self.prob.status, self.var_v)
            if self.has_flux_as_variables:
                # self.solution.x = [self.var_v[r].varValue  for r in self.model.reactions] #deprecated attributes.
                # self.solution.y  = [self.var_y[r].varValue  for r in self.model.reactions].
                self.solution.mu = [self.var_mu[reaction].varValue for reaction in self.model.reactions]
                self.solution.x_dict = {}
                self.solution.y_dict = {}
                self.solution.mu_dict = {}

                for reaction in self.model.reactions:
                    self.solution.x_dict[reaction] = self.var_v[reaction].varValue
                    self.solution.y_dict[reaction] = self.var_y[reaction].varValue
                    self.solution.mu_dict[reaction] = self.var_mu[reaction].varValue

                # Store KOs for convenience.
                self.solution.KOs = [reaction for reaction in self.model.reactions if
                                     self.solution.y_dict[reaction] == 0]

        self.solution.status = self.prob.status

        return self.solution

    def optimize(self):
        pass


def block_reactions(model: cobra.core.Model, reactions: list, cumulative: bool = True) -> cobra.core.Model:
    """BLocks given reactions in the given CobraPy model.

    :param model: cobra.core.Model, model in which reactions will be blocked.
    :param reactions: list, containing reaction IDs of the reaction that need to be blocked.
    :param cumulative: bool, whether previously blocked reactions should remain blocked. Default: True.
    :return: cobra.core.Model, with blocked reactions.
    """

    # Retrieve all provided reactions from the model.
    reaction_list = [reaction for reaction in model.reactions if reaction.id in reactions]

    if model._trimmed_reactions is None:
        model._trimmed_reactions = {}

    if not cumulative:
        unblock_reactions(model)

    # Block reactions.
    for reaction in reaction_list:
        # Running this on an already deleted reaction will overwrite the stored reaction bounds.
        if reaction.id in model._trimmed_reactions:
            continue

        old_lower_bound = reaction.lower_bound
        old_upper_bound = reaction.upper_bound
        model._trimmed_reactions[reaction.id] = (old_lower_bound, old_upper_bound)
        reaction.lower_bound = 0.
        reaction.upper_bound = 0.
        model._trimmed = True

    return model


def evaluate_knock_out(model: cobra.core.Model, target_reaction: str, knock_out_ids: list) \
        -> typing.Tuple[float, float, float]:
    """Predicts the minimal and maximal fluxes of the target reaction and of the biomass reaction based on knock-outs.

    :param model: cobra.core.Model, model for which knock-outs will be predicted.
    :param target_reaction: str, reaction ID of the reaction that is to be optimised.
    :param knock_out_ids: list, Reaction IDs of the reactions that have been knocked out.
    :return: float, minimum flux of the target reaction.
    float, maximum flux of the target reaction.
    float, growth rate of the strain with the knock-out.
    """

    # Create temporary model - make no changes to original model.
    tmpmodel = copy.deepcopy(model)

    # Mutant.
    tmpmodel = block_reactions(tmpmodel, knock_out_ids, cumulative=True)
    tmpmodel.optimize()
    growth_rate = tmpmodel.optimize().objective_value

    # Check if knock-outs are not lethal.
    if growth_rate is not None and growth_rate > 10 ** -6:
        fva = cobra.flux_analysis.flux_variability_analysis(tmpmodel, reaction_list=[tmpmodel.reactions.get_by_id(
            target_reaction)], fraction_of_optimum=1)
        minval = fva.minimum[target_reaction]
        maxval = fva.maximum[target_reaction]
    else:
        minval = None
        maxval = None
        growth_rate = 0

    return minval, maxval, growth_rate


def find_attributes(optknock_model: OptKnock, model: cobra.core.Model, target)\
        -> tuple[float | int, float | int, float | int, list[Any]]:
    """Finds the minimal and maximal fluxes of the target reaction and of the biomass reaction based on knock-outs.
    """

    if hasattr(optknock_model.solution, "KOs"):
        knock_outs = [reaction.id for reaction in optknock_model.solution.KOs]
        # Use FBA to double-check solution.
        minval, maxval, growth_rate = evaluate_knock_out(model, target, knock_outs)
    else:
        minval = 0
        maxval = 0
        growth_rate = 0
        knock_outs = []

    return minval, maxval, growth_rate, knock_outs


def opt_and_rob(model: cobra.core.Model, target: str, number_of_knock_outs: int, knock_out_candidates: set,
                verbose: bool = False) -> typing.Tuple[typing.Dict, typing.Dict]:
    """Performs OptKnock and RobustKnock for the given model for any number of targets.

    :param model: cobra.core.Model, model for which knock-outs will be predicted.
    :param target: str, cobra.Reaction ID that is to be optimised.
    :param number_of_knock_outs: int, The number of allowed knock-outs for OptKnock and RobustKnock.
    :param knock_out_candidates: list, A list of reaction IDs for reactions that can be knocked out.
    Default: all reactions in model.
    :param verbose: bool, indicator of whether intermediate results should be printed to screen. Default: False.
    :return: dict, containing the OptKnock results with {KOs, minimum, maximum, growth_rate} as keys.
    dict, containing the RobustKnock results with {KOs, minimum, maximum, growth_rate} as keys.
    """

    # If no candidate target reactions are given set all reactions in the model as targets.
    if not knock_out_candidates:
        knock_out_candidates = [reaction.id for reaction in model.reactions]

    optknock_model = run_optknock(model, target, number_of_knock_outs, verbose=verbose, integrality_tol=0.,
                                  knock_out_candidate=knock_out_candidates)
    minval, maxval, growth_rate, knock_outs = find_attributes(optknock_model, model, target)
    optknock_result = {"KOs": knock_outs, "minimum": minval, "maximum": maxval, "growth_rate": growth_rate}

    # Run RobustKnock.
    robustknock_model = run_optknock(model, target, number_of_knock_outs, verbose=verbose, tilt=True,
                                     integrality_tol=0., knock_out_candidate=knock_out_candidates)
    minval, maxval, growth_rate, knock_outs = find_attributes(robustknock_model, model, target)
    robustknock_result = {"KOs": knock_outs, "minimum": minval, "maximum": maxval, "growth_rate": growth_rate}

    return optknock_result, robustknock_result


def run_optknock(model: cobra.core.Model, target_reaction: str, num_of_deletions: int, objective_reaction: str = "",
                 objective_fraction: float = 0.2, verbose: bool = True, tilt: bool = False, tilt_value: float = 0.00001,
                 epgap: float = 0.0001, integrality_tol: float = 0,
                 knock_out_candidate: typing.Union[list, list] = None, solver: str = "gurobi") -> OptKnock:
    """Runs the OptKnock algorithm on the given model.

    :param model: cobra.core.Model, model for which knock-outs will be predicted.
    :param target_reaction: str, cobra.Reaction ID of the reaction that is to be optimised.
    :param num_of_deletions: int, The maximum number of allowed knock-outs.
    :param objective_reaction: str, cobra.Reaction ID corresponding to the cellular objective. By default, OptKnock will
    check the cobra model for the objective reaction.
    :param objective_fraction: float, The minimal fraction of normal growth that is maintained.
    :param verbose: bool, A boolean indicator of whether intermediate results should be printed to screen.
    Default: False.
    :param tilt: bool, Whether to tilt the objective. "no" corresponds to the original optimistic OptKnock.
    "yes" corresponds to a pessimistic version more similar to RobustKnock.
    :param tilt_value: float, The size of the (negative!) tilt of the targetRxn to the objective function.
    :param epgap: Float, Acceptable difference between the final result and the dual result.
    :param integrality_tol: float, Maximal deviation of a integer variable from an integer.
    :param knock_out_candidate: list, The reaction IDs of reactions that may be knocked out. Default: all reactions.
    :param solver: str, which solver will be used. Choices: Gurobi, GLPK or glpk.
    :return: OptKnock model with solution.
    """

    assert target_reaction in [reaction.id for reaction in model.reactions], f"Target reaction is not in the model:" \
                                                                             f"{target_reaction!r}."
    assert type(num_of_deletions) is int, f"numDel is not an integer: {num_of_deletions!r}."
    assert objective_reaction in [reaction.id for reaction in model.reactions] or not objective_reaction, \
        f"targetRxn is not in the model: {objective_reaction!r}."
    assert 0 <= objective_fraction <= 1, f"The objective fraction has to be between 0 and 1. The value is:" \
                                         f"{objective_fraction!r}."
    assert verbose is True or verbose is False, f"Verbose has to be True or False."
    assert tilt is True or tilt is False, "Tilt has to be either yes or no."
    assert epgap >= 0, "epgap has to be larger or equal to 0."
    assert integrality_tol >= 0, "integralityTol has to be larger or equal to 0."

    # Set default objective reaction
    if not objective_reaction:
        objective_reaction = [reaction.id for reaction in model.reactions if reaction.objective_coefficient > 0]
        # Code does not work with multiple -> terminate.
        assert len(objective_reaction) == 1, f"There should be exactly one objective reaction. The number of objective"\
                                             f"reactions is: {len(objective_reaction):d}."
        objective_reaction = objective_reaction[0]

    # Initialize model.
    okmodel = OptKnock(model, verbose)

    # Set minimal growth rate.
    if objective_fraction > 0:
        okmodel.model.reactions.get_by_id(objective_reaction).lower_bound = okmodel.model.optimize().objective_value * \
                                                                            objective_fraction

    # Identify knock-outs candidates.
    if knock_out_candidate is not None:
        knock_out_candidate = [reaction for reaction in okmodel.model.reactions if reaction.id in knock_out_candidate]

    # Prepare problem
    okmodel.prepare_optknock(target_reaction, ko_candidates=knock_out_candidate, num_deletions=num_of_deletions,
                             solver_choice=solver, tilt=tilt, tilt_value=tilt_value)

    # Set parameters in solver
    okmodel.prob.solver.epgap = epgap
    # integrality tolerance
    okmodel.prob.solver.integralityTol = integrality_tol

    # Solve problem
    okmodel.solve()

    return okmodel


def unblock_reactions(model: cobra.core.Model) -> cobra.core.Model:
    """Unblocks reactions in a cobraPy model.

    :param model: cobra.core.Model, model in which reactions will be unblocked.
    :return: cobra.core.Model, model with unblocked reactions.
    """

    if model._trimmed_reactions is not None:
        for reaction_ID, (lower_bound, upper_bound) in model._trimmed_reactions.items():
            model.reactions.get_by_id(reaction_ID).lower_bound = lower_bound
            model.reactions.get_by_id(reaction_ID).upper_bound = upper_bound

    model._trimmed_reactions = None
    model._trimmed = False

    return model
