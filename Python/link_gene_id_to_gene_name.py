#!/usr/bin/env python3
""" Changes the gene names in the RNAseq data to the locus tag.
"""

import pandas

__author__ = "Rensco Hogers"


def gene_name_to_locustag(dataframe: pandas.DataFrame, gene_locus_tag_dictionary: dict) -> pandas.DataFrame:
    """ Changes the gene names in the RNAseq data to the locus tag.

    :return: Pandas.DataFrame, The dataframe with the changed gene names.
    """

    for index, row in dataframe.iterrows():
        gene_name = row["target_id"]
        # If gene name is found in the dictionary, change it for the locus tag, else drop the row.
        try:
            dataframe.at[index, "target_id"] = gene_locus_tag_dictionary[gene_name]
        # If gene name is not found, delete the row.
        except KeyError:
            dataframe = dataframe.drop(index)

    return dataframe


def main() -> None:
    """ Main function.
    """

    # Read in the data.
    dictionary_file = "Escherichia_coli_str._K-12_substr._MG1655.gene_info"
    rna_seq_file = "RNAseq_corrected.xlsm"
    gene_locus_tag_dictionary = {}

    with open(dictionary_file) as open_dictionary_file:
        for line in open_dictionary_file:
            # Skip empty lines.
            if not (line := line.strip()):
                continue
            # Skip header line.
            if line.startswith("#"):
                continue
            # Split the line, and extract the gene name and locus tag.
            gene_name, locustag = line.split("\t")[2:4]
            # Add the gene name and locus tag to the dictionary.
            gene_locus_tag_dictionary[gene_name] = locustag

    open_rna_seq_file = pandas.read_excel(rna_seq_file, sheet_name="RNAseq")

    # Change the gene names in the formate data.
    formate_dataframe = open_rna_seq_file[["target_id", "Log2_fold_change_For"]]
    changed_formate_dataframe = gene_name_to_locustag(formate_dataframe, gene_locus_tag_dictionary)

    # Change the gene names in the glycine data.
    glycine_data_frame = open_rna_seq_file[["target_id", "Log2_fold_change_Gly"]]
    changed_glycine_data_frame = gene_name_to_locustag(glycine_data_frame, gene_locus_tag_dictionary)

    # Change the gene names in the pyruvate data.
    pyruvate_dataframe = open_rna_seq_file[["target_id", "Log2_fold_change_Pyr"]]
    changed_pyruvate_dataframe = gene_name_to_locustag(pyruvate_dataframe, gene_locus_tag_dictionary)

    # Write the data to new files.
    formate_rna_seq = "Formate_rna_seq.csv"
    changed_formate_dataframe.to_csv(formate_rna_seq, index=False)

    glycine_rna_seq = "Glycine_rna_seq.csv"
    changed_glycine_data_frame.to_csv(glycine_rna_seq, index=False)

    pyruvate_rna_seq = "Pyruvate_rna_seq.csv"
    changed_pyruvate_dataframe.to_csv(pyruvate_rna_seq, index=False)

    return None


if __name__ == "__main__":
    main()
